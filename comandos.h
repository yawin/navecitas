#ifndef COMANDOS_H
#define COMANDOS_H

#include <RosquilleraReforged/rf_debugconsole.h>
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>

#include "mainprocess.h"
#include "game/actor.h"
#include "game/animation.h"

void cierra(int argc, const char *argv[])
{
	RF_Engine::Status() = false;
}

void taskList(int argc, const char *argv[])
{
  for(RF_Process *p : RF_TaskManager::instance->getTaskList())
  {
    if(p->type != "RF_Text")
    {
        RF_DebugConsoleListener::writeLine(p->id + " -> " + p->type);
    }
  }
}

void loadPackage(int argc, const char *argv[])
{
  if(!RF_AssetManager::isLoaded(string(argv[0])))
  {
    RF_AssetManager::LoadAssetPackage("resources/"+string(argv[0]));
    RF_DebugConsoleListener::writeLine("Paquete de recursos cargado");
  }
  else
  {
    RF_DebugConsoleListener::writeLine("El paquete de recursos ya esta cargado");
  }
}

void unloadPackage(int argc, const char *argv[])
{
  if(RF_AssetManager::isLoaded(string(argv[0])))
  {
    RF_AssetManager::UnloadAssetPackage("resources/"+string(argv[0]));
    RF_DebugConsoleListener::writeLine("Paquete de recursos descargado");
  }
  else
  {
    RF_DebugConsoleListener::writeLine("El paquete de recursos no esta cargado");
  }
}

void setAnimation(int argc, const char *argv[])
{
	string pid = string(argv[0]);
  if(!RF_Engine::existsTask(pid))
  {
    RF_DebugConsoleListener::writeLine("No existe ningun proceso con ese ID");
    return;
  }

  string package = string(argv[1]);
  if(!RF_AssetManager::isLoaded(package))
  {
    RF_DebugConsoleListener::writeLine("No hay cargado ningun paquete de recursos con ese nombre");
    return;
  }

  string filename = string(argv[2]);
  if(!RF_AssetManager::isLoaded(package, filename+"_0"))
  {
    RF_DebugConsoleListener::writeLine("En el paquete seleccionado no existe ninguna animacion con ese nombre");
    return;
  }

  int frameAmount = atoi(argv[3]);
  if(!RF_AssetManager::isLoaded(package, filename+"_"+to_string(frameAmount)))
  {
    RF_DebugConsoleListener::writeLine("La animacion seleccionada no tiene tantos fotogramas");
    return;
  }

  float speed = stof(argv[4]);
  if(speed == 0.0)
  {
    speed = 1.0;
  }

  bool loopea = (strcmp(argv[5], "true")==0);

  if(RF_Engine::getTask<Actor>(pid)->animation == nullptr)
  {
    RF_Engine::getTask<Actor>(pid)->animation = new Animation();
  }
  RF_Engine::getTask<Actor>(pid)->animation->Set(package, filename, frameAmount, speed, loopea);
  RF_DebugConsoleListener::writeLine("Animacion asignada");
}

void GoTo(int argc, const char* argv[])
{
  int scene = atoi(argv[0]);
  MainProcess::instance->ChangeScene(scene);
  RF_DebugConsoleListener::writeLine("Escena cambiada");
}

void setGraph(int argc, const char* argv[])
{
  string pid = string(argv[0]);
  if(!RF_Engine::existsTask(pid))
  {
    RF_DebugConsoleListener::writeLine("No existe ningun proceso con ese ID");
    return;
  }

  string package = string(argv[1]);
  if(!RF_AssetManager::isLoaded(package))
  {
    RF_DebugConsoleListener::writeLine("No hay cargado ningun paquete de recursos con ese nombre");
    return;
  }

  string filename = string(argv[2]);
  if(!RF_AssetManager::isLoaded(package, filename+"_0"))
  {
    RF_DebugConsoleListener::writeLine("En el paquete seleccionado no existe ningun recurso con ese nombre");
    return;
  }
  RF_Engine::getTask(pid)->graph = RF_AssetManager::Get<RF_Gfx2D>(package, filename);
  RF_DebugConsoleListener::writeLine("Grafico asignado");
}

void sendSignal(int argc, const char* argv[])
{
	string pid = string(argv[0]);
  if(!RF_Engine::existsTask(pid))
  {
    RF_DebugConsoleListener::writeLine("No existe ningun proceso con ese ID");
    return;
  }

	RF_Engine::sendSignal(pid, atoi(argv[1]));
}

void generaComandos()
{
  RF_DebugConsoleListener::addCommand("close", new RF_DebugCommand("cierra el juego", 0, &cierra));
  RF_DebugConsoleListener::addCommand("loadPackage", new RF_DebugCommand("carga un paquete de recursos", 1, &loadPackage));
  RF_DebugConsoleListener::addCommand("taskList", new RF_DebugCommand("lista los procesos", 0, &taskList));
  RF_DebugConsoleListener::addCommand("setAnimation", new RF_DebugCommand("asigna una animación a un proceso", 6, &setAnimation));
  RF_DebugConsoleListener::addCommand("goto", new RF_DebugCommand("cambia a la escena indicada", 1, &GoTo));
  RF_DebugConsoleListener::addCommand("setGraph", new RF_DebugCommand("asigna un gráfico a un proceso", 3, &setGraph));
  RF_DebugConsoleListener::addCommand("unloadPackage", new RF_DebugCommand("descarga un paquete de recursos", 1, &unloadPackage));
	RF_DebugConsoleListener::addCommand("sendSignal", new RF_DebugCommand("envía una señal al proceso indicado", 2, &sendSignal));
}
#endif //COMANDOS_H
