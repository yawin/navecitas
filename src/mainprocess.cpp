#include "mainprocess.h"

#include "execontrol.h"
#include "splashscreen.h"
#include "mainmenu.h"
#include "game/level.h"

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>

int startingScene = _SPLASHSCREEN;
bool windowed = false;

MainProcess* MainProcess::instance;

void MainProcess::NextScene()
{
  ChangeScene(actualScene + 1);
}
void MainProcess::ChangeScene(unsigned int scene)
{
  if(scene > _FOO_SCENE || scene == actualScene){return;}
  RF_Engine::sendSignal(stateMachine, S_KILL);

  switch(scene)
  {
    case _SPLASHSCREEN:
      stateMachine = RF_Engine::newTask<SplashScreen>(id);
      break;
    case _MAIN_MENU:
      RF_Engine::Fps(120);
      stateMachine = RF_Engine::newTask<MainMenu>(id);
      break;
    case _GAME:
      stateMachine = RF_Engine::newTask<Level>(id);
      break;
    case _FOO_SCENE:
      RF_Engine::Status() = false;
      break;
  }

  RF_Engine::Debug("[SCENE ENTERING] " + stateMachine);
  actualScene = scene;
}

void MainProcess::Start()
{
  RF_Engine::Debug(windowed);
  RF_Engine::newTask<ExeControl>(id);

  Uint32 windowFlag = (windowed) ? SDL_WINDOW_OPENGL : SDL_WINDOW_OPENGL|SDL_WINDOW_FULLSCREEN;
  window = RF_Engine::addWindow("Navecitas", 1280, 720, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, windowFlag);
  RF_Engine::MainWindow(window);

  ChangeScene(startingScene);
}
