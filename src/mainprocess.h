#ifndef MAINPROCESS_H
#define MAINPROCESS_H

#include <RosquilleraReforged/rf_process.h>

enum Scenes
{
  _SPLASHSCREEN = 0,
  _MAIN_MENU,
  _GAME,
  _FOO_SCENE
};

class MainProcess : public RF_Process
{
  public:
    static MainProcess* instance;

    MainProcess():RF_Process("MainProcess")
    {
      instance = this;
    }

    virtual ~MainProcess(){}

    void ChangeScene(unsigned int scene);
    void NextScene();

    virtual void Start();

  private:
    int actualScene = -1;
    string stateMachine = "";
};

#endif //MAINPROCESS_H
