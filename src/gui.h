#ifndef GUI_H
#define GUI_H

#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_structs.h>
#include <RosquilleraReforged/rf_textmanager.h>

class Button : public RF_Process
{
  public:
    Button():RF_Process("Button"){}
    virtual ~Button()
    {
      if(label != "")
      {
        RF_TextManager::DeleteText(label);
      }
    }

    void Configure(string text, string file, string img, Vector2<float> position, SDL_Color col = {255,255,255})
    {
      gfx = img;
      package = file;
      graph =  RF_AssetManager::Get<RF_Gfx2D>(package, gfx);

      SDL_Rect buttondim = getDimensions();
      transform.position = position;
      transform.position.x-=(buttondim.w*0.5);

      if(text != "")
      {
        label = RF_TextManager::Write<float>(text, col, transform.position);
        RF_Process* lab = RF_Engine::getTask<RF_Process>(label);
        SDL_Rect labeldim = lab->getDimensions();
        Vector2<float> newpos = lab->transform.position;
        newpos.x += (buttondim.w-labeldim.w)*0.5;
        newpos.y += (buttondim.h-labeldim.h)*0.5;
        lab->transform.position = newpos;
      }
    }

    void Update();

    void (*action)(int value);

    private:
        string gfx, package;
        int state = 0;
        string label = "";
};

class Mouse : public RF_Process
{
    public:
      static Mouse *instance;
      Mouse():RF_Process("Mouse")
      {
         instance = this;
      }
      virtual ~Mouse(){instance = nullptr;}

      void Configure(string package, string gfx)
      {
         graph = RF_AssetManager::Get<RF_Gfx2D>(package, gfx);
       }


       void Start();
       void Update();

       bool collision(RF_Process* p);
       bool OnClick = false;
};

extern string gui_package;
extern string gui_font;
extern string gui_mouse;
void initGUI(string path, string font = "", string display = "");
void stopGUI(string display = "");

template<typename T = float>
string addButton(string text, Vector2<T> position, void (*event)(int value) = nullptr, string display = "")
{
  string b = RF_Engine::newTask<Button>(display);
  Button *but = RF_Engine::getTask<Button>(b);
  but->action = event;

  RF_TextManager::Font = RF_AssetManager::Get<RF_Font>(gui_package, gui_font);
  but->Configure(text, gui_package, "button", position);
  return b;
}

#endif //GUI_H
