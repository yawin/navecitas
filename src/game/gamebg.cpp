#include "game/gamebg.h"
#include "game/actor.h"

#include <RosquilleraReforged/rf_assetmanager.h>

GameBG* GameBG::instance = nullptr;
GameBG::GameBG():RF_Layer()
{
  if(instance != nullptr)
  {
    signal = S_KILL;
    return;
  }

  instance = this;
}
void GameBG::Set(string package, string img)
{
  setWindow(RF_Engine::getWindow(RF_Engine::MainWindow()));
  graph = RF_AssetManager::Get<RF_Gfx2D>(package, img);

  lims.x = RF_Engine::MainWindow()->width()>>1;
  SDL_Rect dim = getDimensions();
  lims.y = dim.w-lims.x;
}

void GameBG::SetCamera(string cam)
{
  camID = cam;
}

void GameBG::Draw()
{
  if(camID == ""){return;}

  Actor* camera = RF_Engine::getTask<Actor>(camID);
  if(camera != nullptr && !blocked)
  {
    if(camera->realPosition.x > (lims.x) && camera->realPosition.x < lims.y)
    {
      transform.position.x = -(camera->realPosition.x-lims.x);
    }
    else if(camera->realPosition.x <= (lims.x))
    {
      transform.position.x = 0;
    }
    else if(camera->realPosition.x >= lims.y)
    {
      //transform.position.x = lims.y;
    }
  }
}
