#ifndef ENEMY_H
#define ENEMY_H

#include "game/actor.h"

class Enemy : public Actor
{
  public:
    Enemy(string hiddenType, string pid = "enemy");
    virtual ~Enemy(){}

    virtual void OnUpdate();

    string enemyType;
    CallBackFunction enemyLogic = nullptr;
};

#endif //ENEMY_H
