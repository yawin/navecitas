#ifndef MOVEMENTS_H
#define MOVEMENTS_H

#include "game/actor.h"

void linearMovement(Actor* entity);

void BasicMovement(Actor* entity);
void EyeMovement(Actor* entity);

void PlayerMovement(Actor* entity);

#endif //MOVEMENTS_H
