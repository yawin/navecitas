#include "enemy.h"
#include "game/advancer.h"
#include <RosquilleraReforged/rf_engine.h>

Enemy::Enemy(string hiddenType, string pid):Actor(pid)
{
  enemyType = hiddenType;
  blocked = true;
}

void Enemy::OnUpdate()
{
  if(blocked)
  {
    blocked = !(realPosition.x - Advancer::instance->realPosition.x < (float)(RF_Engine::MainWindow()->width()>>1));
  }
  else
  {
    if(enemyLogic != nullptr)
    {
      enemyLogic();
    }
  }
}
