#ifndef LEVEL_H
#define LEVEL_H

#include "game/gamebg.h"
#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_engine.h>

typedef void (*LevelFunction)();
struct LevelData
{
  LevelFunction Start;
  LevelFunction Update;
  LevelFunction End;

  LevelData(LevelFunction s, LevelFunction u, LevelFunction e)
  {
    Start = s;
    Update = u;
    End = e;
  }
};

class Level : public RF_Process
{
  public:
    static Level* instance;
    Level():RF_Process("level")
    {
      instance = this;
    }
    virtual ~Level(){}

    virtual void Start();
    virtual void Update();
    void InitLevel();
    void StopLevel();

    static void NextLevel();
    static void Reload();
    static int map;

    static struct LevelData levels[];
    int levelAmount = 2;
    string spawner = "";
};

void Level0Start();

void FooFunction();
#endif //LEVEL1_H
