#include "game/bullets.h"

#include "game/movements.h"

void StandardBullet::OnSpawn()
{
  setAnimation("onSpawn", "bullets", "standard_spawn", 3, 3.0, false);
  setAnimation("onDamage", "bullets", "standard", 1, 1.0);
  lives = 1;
  speed = 100;
  moveStrategy = &linearMovement;
}

void StandardBullet::OnUpdate()
{
  if(!changed && !animation->canExecute())
  {
    changed = true;
    DoDamage(0);
  }
}
