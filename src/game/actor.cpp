#include "game/actor.h"
#include <RosquilleraReforged/rf_engine.h>

void Actor::SetBG(string bgID)
{
  SetBG(RF_Engine::getTask<GameBG>(bgID));
}
void Actor::SetBG(GameBG* bg)
{
  background = bg;
}

void Actor::SetPosition(float x, float y)
{
  realPosition.x = x;
  realPosition.y = y;
}
void Actor::SetPosition(Vector2<float> pos)
{
  SetPosition(pos.x, pos.y);
}
void Actor::Move(float x, float y)
{
  realPosition.x += x;
  realPosition.y += y;
}
void Actor::Move(Vector2<float> pos)
{
  Move(pos.x, pos.y);
}

void Actor::Draw()
{
  if(animation != nullptr && animation->canExecute())
  {
    SDL_Surface* gfx = animation->getFrame();
    if(gfx != nullptr)
    {
      graph = gfx;
    }
  }
}

void Actor::LateDraw()
{
  if(background != nullptr)
  {
    transform.position = Vector2<float>(realPosition.x + background->transform.position.x - (graph->w>>1), realPosition.y + background->transform.position.y - (graph->h>>1));
  }
}

void Actor::Start()
{
  SetBG(GameBG::instance);
  animation = new Animation();

  OnSpawn();
  tryAnimation("onSpawn");
}

void Actor::Update()
{
  OnUpdate();

  if(blocked){return;}

  if(moveStrategy != nullptr)
  {
    moveStrategy(this);
  }
}

SDL_Rect Actor::normalizeBounds(const SDL_Rect& rect)
{
  if(graph == nullptr){return {(int)transform.position.x, (int)transform.position.y, 0, 0};}

  SDL_Rect normalized;
  normalized.x = rect.x - (Sint16)transform.position.x;// + (Sint16)(graph->w >> 1);
  normalized.y = rect.y - (Sint16)transform.position.y;// + (Sint16)(graph->h >> 1);
  normalized.w = rect.w;
  normalized.h = rect.h;

  return normalized;
}

void Actor::setAnimation(string aID, const char* b, int f, float s, bool l, CallBackFunction c)
{
  setAnimation(aID, "", b, f, s, l, c);
}
void Actor::setAnimation(string aID, const char* p, const char* b, int f, float s, bool l, CallBackFunction c)
{
  states[aID] = new StateGfx(p,b,f,s,l,c);
}
void Actor::tryAnimation(string aID)
{
  if(states[aID] != nullptr)
  {
    animation->Set(states[aID]->package, states[aID]->basename, states[aID]->frameAmount, states[aID]->speed, states[aID]->looped, states[aID]->callback);
  }
}
void Actor::DoDamage(int amount)
{
  lives-= amount;
  if(lives <= 0)
  {
      moveStrategy = nullptr;
      tryAnimation("onDead");
      OnDead();
  }
  else
  {
    tryAnimation("onDamage");
    OnDamage();
  }
}

void Shooter::Update()
{
  Actor::Update();

  if(shootStrategy != nullptr)
  {
    if(shootCoolDown <= 0.0)
    {
      shootStrategy(this);

      if(type != "prota")
      {
        shootCoolDown = shootingSpeed;
      }
    }
    else
    {
      shootCoolDown-=RF_Engine::instance->Clock.deltaTime;
    }
  }
}
