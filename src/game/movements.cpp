#include "game/movements.h"
#include "game/gamebg.h"
#include "game/enemies.h"

#include <RosquilleraReforged/rf_engine.h>

void linearMovement(Actor* entity)
{
  entity->realPosition.x += entity->moveDirection.x*entity->speed*RF_Engine::instance->Clock.deltaTime;
  entity->realPosition.y += entity->moveDirection.y*entity->speed*RF_Engine::instance->Clock.deltaTime;
}

void BasicMovement(Actor* entity)
{
  return;
}

void EyeMovement(Actor* entity)
{
  Eye *e = reinterpret_cast<Eye*>(entity);

  if(e->first)
  {
    Eye *lptr = e->nextFriend, *ptr=e;
    Vector2<float> bak;
    while(ptr != nullptr)
    {
      if(lptr!= nullptr){lptr->nextPos = ptr->realPosition;}
      ptr->realPosition = ptr->nextPos;
      ptr = ptr->nextFriend;
      if(lptr!= nullptr){lptr = lptr->nextFriend;}

    }

    e->step += RF_Engine::instance->Clock.deltaTime;
    e->realPosition.x = e->startingPos.x + cos(3.14*e->step*e->modif.x)*e->speed;
    e->realPosition.y = e->startingPos.y + sin(3.14*e->step*e->modif.y)*e->speed;
  }
}

void PlayerMovement(Actor* entity)
{
  if(entity->realPosition.x-(entity->getDimensions().w>>1) < -GameBG::instance->transform.position.x)
  {
    entity->realPosition.x = -(GameBG::instance->transform.position.x-(entity->getDimensions().w>>1));
  }

  if(!RF_Engine::getWindow(entity->window)->hasFocus()){return;}

  float tmpSpeed = entity->speed*RF_Engine::instance->Clock.deltaTime;
  if(RF_Input::key[_w] && !RF_Input::key[_s])
  {
    if(entity->transform.position.y-tmpSpeed < 0)
    {
      tmpSpeed = entity->transform.position.y;
    }

    entity->Move(0, -tmpSpeed);
  }

  if(RF_Input::key[_s] && !RF_Input::key[_w])
  {
    if(entity->transform.position.y+tmpSpeed > (RF_Engine::MainWindow()->height()-entity->getDimensions().h))
    {
      tmpSpeed = (RF_Engine::MainWindow()->height()-entity->getDimensions().h) - entity->transform.position.y;
    }

    entity->Move(0, tmpSpeed);
  }

  if(RF_Input::key[_a] && !RF_Input::key[_d])
  {
    if(entity->transform.position.x-tmpSpeed*0.5 < 0)
    {
      tmpSpeed = entity->transform.position.x;
    }

    entity->Move(-tmpSpeed*0.5, 0);
  }

  if(RF_Input::key[_d] && !RF_Input::key[_a])
  {
    if(entity->transform.position.x+tmpSpeed > (RF_Engine::MainWindow()->width()-entity->getDimensions().w))
    {
      tmpSpeed = (RF_Engine::MainWindow()->width()-entity->getDimensions().w) - entity->transform.position.x;
    }

    entity->Move(tmpSpeed, 0);
  }
}
