#include "game/prota.h"
#include "game/movements.h"
#include "game/shoots.h"
#include "game/animation.h"

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_taskmanager.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_collision.h>
#include "game/level.h"

Prota* Prota::instance = nullptr;

void Prota::OnSpawn()
{
  zLayer = background->zLayer-1;
  instance = this;
  speed = 220;
  //background->SetCamera(id);

  setAnimation("onSpawn", "prota", "nave", 7, 14.0);
  setAnimation("onDead", "prota", "explosion", 6, 5.0, false, &Level::Reload);

  lives = 1;
  SetPosition(RF_Engine::MainWindow()->width()>>1, RF_Engine::MainWindow()->height()>>1);
  moveStrategy = &PlayerMovement;
  shootStrategy = &PlayerShooting;
}

void Prota::OnUpdate()
{
  if(lives > 0)
  {
    if(invuln <= 0.0)
    {
      RF_Process* collided = checkCollisionByType(RF_Engine::getTask<RF_Process>(id), "enemy");
      if(collided)
      {
        DoDamage();
      }
    }
    else
    {
      invuln-=RF_Engine::instance->Clock.deltaTime;
    }
  }
}
