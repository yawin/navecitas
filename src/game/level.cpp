#include "game/level.h"
#include "game/prota.h"
#include "game/enemies.h"
#include "game/advancer.h"
#include "mainprocess.h"

#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_taskmanager.h>

int Level::map = 0;
Level* Level::instance = nullptr;

void Level::Start()
{
  if(!RF_AssetManager::isLoaded("prota"))
  {
    RF_AssetManager::LoadAssetPackage("resources/prota");
  }
  if(!RF_AssetManager::isLoaded("enemies"))
  {
    RF_AssetManager::LoadAssetPackage("resources/enemies");
  }
  if(!RF_AssetManager::isLoaded("bullets"))
  {
    RF_AssetManager::LoadAssetPackage("resources/bullets");
  }

  RF_Engine::newTask<GameBG>(id);
  InitLevel();
}

void Level::Update()
{
  if(spawner != "")
  {
    levels[map].Update();
  }
}

void Level::NextLevel()
{
  if(++Level::instance->map < Level::instance->levelAmount)
  {
    Level::Reload();
  }
}

void Level::Reload()
{
  Level::instance->StopLevel();
  Level::instance->InitLevel();
}

void Level::InitLevel()
{
  RF_AssetManager::LoadAssetPackage("resources/level"+to_string(map));
  GameBG::instance->Set("level"+to_string(map));

  spawner = RF_Engine::newTask<RF_Process>(id);
  RF_Engine::newTask<Advancer>(spawner);
  levels[map].Start();
  RF_Engine::newTask<Prota>(spawner);
}

void Level::StopLevel()
{
  RF_Engine::sendSignal(spawner, S_KILL);
}

LevelData Level::levels[] = {
  LevelData(&Level0Start, &FooFunction, &FooFunction),
  LevelData(&FooFunction, &FooFunction, &FooFunction)
};

void Level0Start()
{
  Eye::Squad(Vector2<float>(RF_Engine::MainWindow()->width(), RF_Engine::MainWindow()->height()>>1), Level::instance->spawner);
  Eye::Squad(Vector2<float>(RF_Engine::MainWindow()->width()+640, 400), Level::instance->spawner, 350, Vector2<float>(0.01, 0.10));
  Eye::Squad(Vector2<float>(RF_Engine::MainWindow()->width()+1240, 400), Level::instance->spawner, 350, Vector2<float>(0.2, 0.36));
}

void FooFunction(){}
