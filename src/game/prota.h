#ifndef PROTA_H
#define PROTA_H

#include "game/actor.h"

class Prota : public Shooter
{
  public:
    Prota():Shooter("prota"){}
    virtual ~Prota(){}

    static Prota* instance;

    virtual void OnSpawn();
    virtual void OnUpdate();

    float invuln = 1.0;
};

#endif //PROTA_H
