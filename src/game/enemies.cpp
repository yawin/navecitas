#include "enemies.h"
#include "game/movements.h"
#include "game/level.h"

void Eye::OnSpawn()
{
  setAnimation("onSpawn", "enemies", "eye", 3, 8.0);
  moveStrategy = &EyeMovement;
}

void Eye::Squad(Vector2<float> position, string fatherID, float speed, Vector2<float> modif)
{
  string ship = "";
  Eye *sh, *lsh;
  Vector2<float> realPos;
  realPos.x = position.x + cos(3.14*0*modif.x)*speed;
  realPos.y = position.y + sin(3.14*0*modif.y)*speed;

  for(float i = 0.0; i < 1.0; i+=0.1)
  {
    ship = RF_Engine::newTask<Eye>(Level::instance->spawner);
    sh = RF_Engine::getTask<Eye>(ship);
    //sh->SetPosition(position);
    sh->nextPos = realPos;
    sh->zLayer = (int)(i*10);

    if(i != 0.0)
    {
      lsh->nextFriend = sh;
    }
    else
    {
      sh->startingPos = position;
      sh->first = true;
      sh->speed = speed;
      sh->modif = modif;
    }

    lsh = sh;
  }
}
