#ifndef ENEMIES_H
#define ENEMIES_H

#include "enemy.h"

class Eye : public Enemy
{
  public:
    Eye():Enemy("eye"){}
    virtual ~Eye(){}

    virtual void OnSpawn();
    float step = 0.0;
    Eye* nextFriend = nullptr;
    Vector2<float> nextPos, startingPos;
    bool first = false;
    Vector2<float> modif;

    static void Squad(Vector2<float> position, string fatherID, float speed = 340, Vector2<float> modif = Vector2<float>(0.25, 0.2));
};

#endif //ENEMIES_H
