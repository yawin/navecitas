#ifndef GAMEBG_H
#define GAMEBG_H

#include <RosquilleraReforged/rf_layer.h>

class GameBG : public RF_Layer
{
  public:
    static GameBG *instance;

    GameBG();

    virtual ~GameBG()
    {
      instance = nullptr;
    }

    void Set(string package, string img = "bg");
    void SetCamera(string cam);

    virtual void Draw();

  private:
    Vector2<float> lims;
    bool blocked = false;
    string camID = "";
};

#endif //GAMEBG_H
