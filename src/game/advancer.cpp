#include "advancer.h"
#include "movements.h"

Advancer* Advancer::instance = nullptr;

void Advancer::OnSpawn()
{
  instance = this;
  speed = 50;
  moveStrategy = &linearMovement;
  background->SetCamera(id);
  SetPosition(RF_Engine::MainWindow()->width()>>1, RF_Engine::MainWindow()->height()>>1);
}

void Advancer::OnUpdate()
{
  if(delay > 0.0)
  {
    blocked = true;
    delay-=RF_Engine::instance->Clock.deltaTime;
  }
  else
  {
    blocked = false;
  }
}

void Advancer::setDelay(float d)
{
  delay = d;
}
