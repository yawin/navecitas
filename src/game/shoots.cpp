#include "game/shoots.h"
#include "game/bullets.h"
#include <RosquilleraReforged/rf_engine.h>

void PlayerShooting(Shooter* entity)
{
  if(RF_Input::key[_space])
  {
    Actor* b = RF_Engine::getTask<Actor>(RF_Engine::newTask<StandardBullet>(entity->id));
    b->realPosition.x = entity->shootDirection.x*(entity->getDimensions().w>>1) + entity->realPosition.x;
    b->realPosition.y = entity->shootDirection.y*(entity->getDimensions().h>>1) + entity->realPosition.y;
    b->moveDirection = entity->shootDirection;
    entity->ShootCoolDown() = entity->ShootingSpeed();
  }
}
