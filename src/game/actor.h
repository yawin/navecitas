#ifndef ACTOR_H
#define ACTOR_H

#include <RosquilleraReforged/rf_process.h>
#include "game/gamebg.h"
#include "game/animation.h"

#include <map>
using namespace std;

typedef void (*CallBackFunction)();

struct StateGfx
{
  string package, basename;
  int frameAmount;
  float speed;
  bool looped;
  CallBackFunction callback = nullptr;

  void save(const char* p, const char* b, int f, float s, bool l, CallBackFunction c)
  {
      package = string(p);
      basename = string(b);
      frameAmount = f;
      speed = s;
      looped = l;
      callback = c;
  }

  StateGfx(const char* p, const char* b, int f, float s = 1.0, bool l=true, CallBackFunction c = nullptr)
  {
    save(p, b, f, s, l, c);
  }
  StateGfx(const char* b, int f, float s = 1.0, bool l=true, CallBackFunction c = nullptr)
  {
    save("", b, f, s, l, c);
  }
};

class Actor : public RF_Process
{
  public:
    Actor(string name = "actor"):RF_Process(name){}
    virtual ~Actor(){}

    void SetBG(string bgID);
    void SetBG(GameBG* bg);
    void SetPosition(float x, float y);
    void SetPosition(Vector2<float> pos);
    void Move(float x, float y);
    void Move(Vector2<float> pos);

    Vector2<float> realPosition;

    virtual void Start();
    virtual void Update();
    void (*moveStrategy)(Actor* entity) = nullptr;

    virtual void Draw();
    virtual void LateDraw();
    virtual SDL_Rect normalizeBounds(const SDL_Rect& rect);

    virtual void DoDamage(int amount = 1);

    virtual void OnSpawn(){}
    virtual void OnUpdate(){}
    virtual void OnDamage(){}
    virtual void OnDead(){}
    float speed = 0;

    Animation* animation = nullptr;
    Vector2<float> moveDirection = Vector2<float>(1.0,0.0);

  protected:
    bool blocked = false;
    int lives = 1;
    GameBG* background = nullptr;
    map<string, StateGfx*> states;

    void tryAnimation(string aID);
    void setAnimation(string aID, const char* b, int f, float s = 1.0, bool l=true, CallBackFunction c = nullptr);
    void setAnimation(string aID, const char* p, const char* b, int f, float s = 1.0, bool l=true, CallBackFunction c = nullptr);
};

class Shooter : public Actor
{
  public:
    Shooter(string name = "shooter"):Actor(name){}
    virtual ~Shooter(){}

    virtual void Update();

    void (*shootStrategy)(Shooter* entity) = nullptr;

    Vector2<float> shootDirection = Vector2<float>(1.0, 0.0);
    inline float& ShootCoolDown(){return shootCoolDown;}
    inline float& ShootingSpeed(){return shootingSpeed;}

  protected:
    float shootingSpeed = 0.25, shootCoolDown = 0.0;
};

#endif //ACTOR_H
