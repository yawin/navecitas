#ifndef ANIMATION_H
#define ANIMATION_H

#include <RosquilleraReforged/rf_assetmanager.h>
#include <string>
using namespace std;

class Animation
{
  public:
    Animation(){}
    virtual ~Animation(){}

    void Set(string packagename, string filename, int amount, float animationSpeed = 1.0, bool repeating = true, void (*event)() = nullptr)
    {
      if(RF_AssetManager::isLoaded(packagename, filename))
      {
        package = packagename;
        name = filename;
        framesAmount = amount;
        speed = animationSpeed;
        repeat = repeating;
        callBack = event;
        actualFrame = 0.0;
      }
      else
      {
        RF_Engine::Debug("No puedo cargar " + packagename + ", " + filename);
      }
    }

    bool canExecute(){return (name != "");}

    SDL_Surface* getFrame()
    {
      SDL_Surface* ret = nullptr;
      if(name != "")
      {
        actualFrame+=(speed*RF_Engine::instance->Clock.deltaTime);
        if(actualFrame < 0.0){actualFrame = 0.9+((float)framesAmount);}
        int aF = (int)actualFrame;

        if(aF > framesAmount)
        {
          actualFrame = 0.0;
          if(!repeat)
          {
            name = "";
            if(callBack != nullptr)
            {
              callBack();
              return nullptr;
            }
          }
          else
          {
            actualFrame+=(speed*RF_Engine::instance->Clock.deltaTime);
            aF = (int)actualFrame;
          }
        }

        ret = RF_AssetManager::Get<RF_Gfx2D>(package, name+"_"+to_string(aF));
      }

      return ret;
    }

  private:
    bool repeat = true;
    string name = "", package = "";
    int framesAmount = 0;
    float actualFrame = 0.0;
    float speed = 1.0;
    void (*callBack)();
};
#endif //ANIMATION_H
