#ifndef BULLETS_H
#define BULLETS_H

#include "game/actor.h"

class StandardBullet : public Actor
{
  public:
    StandardBullet(string pid = "StandardBullet"):Actor(pid){}
    virtual ~StandardBullet(){}

    virtual void OnSpawn();
    virtual void OnUpdate();

    string target = "enemy";

  private:
    bool changed = false;
};

#endif //BULLETS_H
