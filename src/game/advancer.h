#ifndef ADVANCER_H
#define ADVANCER_H

#include "game/actor.h"

class Advancer : public Actor
{
  public:
    static Advancer* instance;

    Advancer():Actor("advancer"){}
    virtual ~Advancer()
    {
      if(instance == this){instance = nullptr;}
    }

    virtual void OnSpawn();
    virtual void OnUpdate();

    void setDelay(float d);

  private:
    float delay = 0.0;
};

#endif //ADVANCER_H
