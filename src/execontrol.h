#ifndef EXECONTROL_H
#define EXECONTROL_H

#include "mainprocess.h"
#include "game/animation.h"
#include "game/actor.h"

#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_structs.h>
#include <RosquilleraReforged/rf_input.h>
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_taskmanager.h>

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

class ExeControl : public RF_Process
{
  public:
    ExeControl():RF_Process("ExeControl"){}
    virtual ~ExeControl(){}

    virtual void Update()
    {
      if(RF_Input::key[_esc] || RF_Input::key[_close_window])
      {
        RF_Engine::Status() = false;
      }
    }
};

#endif //EXECONTROL_H
