#ifndef MAINMENU_H
#define MAINMENU_H

#include <RosquilleraReforged/rf_process.h>

class MainMenu : public RF_Process
{
  public:
    MainMenu():RF_Process("MainMenu"){}
    virtual ~MainMenu();

    virtual void Start();
};

#endif //MAINMENU_H
