#include "mainmenu.h"
#include "mainprocess.h"

#include "gui.h"
MainMenu::~MainMenu()
{
  stopGUI();
}

void MainMenu_StartButton(int foo)
{
  MainProcess::instance->NextScene();
}
void MainMenu_ExitButton(int foo)
{
  RF_Engine::Status() = false;
}

void MainMenu::Start()
{
  zLayer = 1001;
  initGUI("gui", "Debby", id);
  graph = RF_AssetManager::Get<RF_Gfx2D>("gui", "mainmenu_background");


  RF_Window* w = RF_Engine::MainWindow();
  addButton("Empezar", Vector2<float>(w->width()*0.5, w->height()*0.55), &MainMenu_StartButton, id);
  addButton("Salir", Vector2<float>(w->width()*0.5, (w->height()*0.55) + 110), &MainMenu_ExitButton, id);
}
